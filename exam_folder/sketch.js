// Declare global variables.
// If these were inside a function you could only call upon them in that specific function not outside of it (local variables).

// Variable which contains the names of all the input fields
let inputFields = [];
let mode = 0 //Start the mode as 0, aka screen 1
let nameInput, genderInput, zipcodeInput, mentalInput, sinInput, ageInput, occupationInput, locationInput, parentInput, googleInput; //Here we declare the variables for each input box
let logo; //The logo's variable
let row1 = 30;
let row2 = 345;
let textPrefixes = ["Your name is ","You identify as ", "Your zipcode is ", "Your answer to your mental stability was: ", "Your latest sin was ", "You were born in ", "Your occupation is ","Your current location is ", "You prefer ", "You googled that because: "];
// Preload the images to prevent lag.
function preload() {
	logo = loadImage('assets/borgerdk2.png');
}

function setup() {
	createCanvas(550, 675);

	//Create the button, which will change from screen 1 to screen 2
	submitButton = createButton('Submit');
	submitButton.position(210, 620);
	submitButton.size(80,32);
	submitButton.mousePressed(openBlackBox);
	submitButton.style("background", "#44831E");
	submitButton.style("color", "#FFF");
	submitButton.style("border", "none");
	submitButton.style("font-size", "1.1em");
  submitButton.style("font-weight", "normal");
	submitButton.style("border-radius", "3px");
	submitButton.style("text-align", "center");
	submitButton.style("font-family", "Arial");
	//Changes the background colour of the button if the mouse is hovering over it
	submitButton.mouseOver(changeBlue);
	submitButton.mouseOut(changeBack);


	//Here we insert the input boxes, first row
	nameInput = createInput("");
	nameInput.position(row1, 240);
	nameInput.size(100);

	genderInput = createInput("");
	genderInput.position(row1, 320);
	genderInput.size(100);

	zipcodeInput = createInput("");
	zipcodeInput.position(row1, 400);
	zipcodeInput.size(100);

	mentalInput = createInput("");
	mentalInput.position(row1, 480);
	mentalInput.size(100);

	sinInput = createInput("");
	sinInput.position(row1, 560);
	sinInput.size(100);

	//second row of input boxes
	ageInput = createInput("");
	ageInput.position(row2, 240);
	ageInput.size(100);

	occupationInput = createInput("");
	occupationInput.position(row2, 320);
	occupationInput.size(100);

	locationInput = createInput("");
	locationInput.position(row2, 400);
	locationInput.size(100);

	parentInput = createInput("");
	parentInput.position(row2, 480);
	parentInput.size(100);

	googleInput = createInput("");
	googleInput.position(row2, 560);
	googleInput.size(100);

	//Adds all input fields to list inputFields
	inputFields.push(nameInput, genderInput, zipcodeInput, mentalInput, sinInput, ageInput, occupationInput, locationInput, parentInput, googleInput);
}

function draw() {
  background(220);

	//Here we change from screen 1 to screen 2, based on the "submit" button action (openBlackBox).
  if (mode == 0) {
    screen1();
  } else if (mode == 1) {
    screen2();
  }

}


//Useful to have multiple elements on one screen
function screen1() {
	push();
	fill(250);
  strokeWeight(0);
	rect(0, 125, 550, 610);
	pop();

	push();
	fill(240);
  strokeWeight(0);
	rect(0, 125, 550, 70);
	pop();

	push();
	imageMode(CENTER);
	image(logo, 275, 65);
	logo.resize(500, 0);
	pop();

	//Greeting
	push();
	textFont("Arial");
	textSize(14);
	text("Welcome to borger.kb!", 200, 150);
	text("Please answer all of the questions, so we can update our information about you.", 27, 175);
	pop();

	//Here we write our questions, which will be above the input boxes, row 1
	textSize(14);
	text("What is your name?", row1, 230);
	text("What is your gender?", row1, 310);
	text("What is your zip-code?", row1, 390);
	text("Are you mentally stable?", row1, 470);
	text("What was your last sin?", row1, 550);

	//row 2
	text("When were you born?", row2, 230);
	text("What is your occupation?", row2, 310);
	text("Where are you right now?", row2, 390);
	text("Which parent do you prefer?", row2, 470);
	text("Why did you google that?", row2, 550);


}

function screen2() {
	background(100);

	//Here we delete the "submit" button and the input boxes
	// Removes all input fields
	for(let i = 0; i < inputFields.length; i++){
		inputFields[i].remove();
	}
	submitButton.remove();
	//Writes text
  fill(255);
  textSize(16);
  textAlign(LEFT, TOP);
  noStroke();


  //Redacted text function
  for(let i = 0; i < inputFields.length; i++){
    var wordWidth = textWidth(inputFields[i].value());
    var boxOffset = textWidth(textPrefixes[i]);
    fill(0);
    rect(25 + boxOffset, 25+i*20, wordWidth, 16); //last input here has to be the same as text size.
    if (mouseIsPressed) {
      fill(255);
      text (inputFields[i].value(), 25 + boxOffset, 25+i*20);
    }
    fill(255);
    text(textPrefixes[i], 25, 25+i*20);
  }
}



/*This function reacts to the "submit" button.
It changes the mode from 0 to 1, which then changes the "screen" (see draw function)*/
function openBlackBox() {
	for(let i = 0; i < inputFields.length; i++){
		//Check if the called input is empty or if anything has been written
		if (inputFields[i].value() == ''){
			// Makes a pop-up if any field was empty. Then it breaks the loop.
			window.alert("You need to fill all the fields before proceeding.");
			return;
		}
	}
	mode = 1;
}


//These functions change the colour of the button
function changeBlue() {
  submitButton.style("background", "#304C60");
  }

function changeBack() {
		submitButton.style("background", "#44831E");
	}
