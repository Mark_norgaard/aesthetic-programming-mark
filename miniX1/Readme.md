# miniX1 Mark Nørgaard

### IT SNOWS! *Okay maybe just a little*
_Link_ https://mark_norgaard.gitlab.io/aesthetic-programming-mark/miniX1/

![screenshot](Screenshot_2.png)

So uhm, I've made a sketch where some 40 elipses fall down form the top of the canvas and sort of look like snow.
The 40 ellipses are randomly generated along the x axis to the end of my canvas all with y = 0.
Then they drop by 2 pixels per frame (so y+2). It's 120 pixels per second.
I have also made them *shake* a little so they look like snowflakes.
I made the sketch with the purpose of learning about classes so I could "draw" 40 different snowflakes that were all their own object so to speak.
This was with the intention of being able to do different things to all of them without affecting the rest of the snowflakes.
I then ended up learning about for loops and arrays as well.
Arrays are in essence lists, in the lists there are certain points. In this case it's different instantiations of my class the snowflakes.
For loops in this sketch are used to count the number of repetitions to exucute a certain code. As an example in the *function setup* part of the code, the for loop is set to repeat the underlying code untill a lenth of 40 things is reached in the list *sne*.

### _How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?_

Here I'm supposed to descibe my first independant coding experience, but my memory fails me. I have coded before in gymnasium, some 3 years back now.
Though I must admit that just as remembering my first time coding, my memory of *how* to code had also faltered.
Most of the time spent here was spent relearning and reremembering different syntaxes and how to use them. I was at a basic level back then,
but I must admit I was more stumped by coding than I expected to be this time. Though it all ended up working out, with a little help from Daniel schiffmans video called **mouse interaction with objects** *I should absolutely just have watchet his video on arrays of classes*.

### _How is the coding process different from, or similar to, reading and writing text?_

Where the process of coding differs for me from reading or writing is primarily in the ridigness of the process. When writing a story you get to choose with which method you wish to reach a certain conclusion, the conclusion can even be unevident to you and also in the making. When coding it is best for me to set out with a plan. I want to reach this specific goal, and I'm going to use these methods *syntaxes* to reach it. It is more akin to math than prose I would argue. In math a task is at hand, and there are known processes to reach that goal, and if you don't know the processes with which to reach it you can simply study and find out. With coding it is very similar. You set a goal, figure out how to reach it, execute the plan.

### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?

Coding to me has always previously been a tool. Much like being good as maths, being good at coding lets you use the tool to reach goals. It is akin to any other theoretical craft, perhaps even physical craftsmanship, though that agument may be a slippery slope. The assigned reading helps me consider coding as more than just a tool. It becomes a cultural force, a literracy, perhaps it is even dangerous to be unknowing with relation to code.

Well that took a dark turn, but it is in essence to me still a tool. And being a tool of large cultural impact like a hammer or a nail, it befits most people to know a bit about it.
