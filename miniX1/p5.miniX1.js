let sne = [];

function setup () {
  createCanvas(800, 1200);
  background(51);
  for (let i = 0; i < 40; i++){
    let x = random(width);
    let y = 0;
    let s = new Snefnug(x, y);
    sne.push(s);
  }
}

function draw() {
    background(51);
    for (let i = 0; i < sne.length; i++) {
      sne[i].move();
      sne[i].show();
    }
}

class Snefnug {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  move() {
    this.x = this.x + random(-2 , 2);
    this.y = this.y + 2;
  }

  show() {
    stroke(255);
    ellipse(this.x, this.y, 10, 10);
    fill(255);
  }
}
