let face;
let hand;
let faces = [];
let hands = [];
let winW = window.innerWidth;
let winH = window.innerHeight;
let hm = 0;
let wf = 200;
let hf = 200;
let wh = 150*2;
let hh = 150;
let yf = winH/2;
let yh = (winH/2)+50;

function preload() {
  face = loadImage('Face.png');
  hand = loadImage('Hand.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(51);
  imageMode(CENTER);
  frameRate(10);
}

function keyTyped() {
  if (key === 'h') {
    hm = hm + 1;
  }

  if (key === 'M') {
    hf = hf * 1.1;
    hh = hh * 1.1;
  }

  if (key === 'm') {
    hf = hf * 0.9;
    hh = hh * 0.9;
  }

  if (key === 'd') {
  //  yh = yh + 200;
    yf = yf + 200;
    yh = yh + 200;
  }

  if (key == 'u') {
    yf = yf - 200;
    yh = yh - 200;
  }
}


function draw() {
  // image(face, 50, 50, 200, 200);
  // rotate(0);
  // image(hand, 80, 140, 120, 120);

  for (let i = 0; i < hm; i++) {
    let xf = winW/(1/((i+1)/(hm+1)));
    let f = new Face(xf, yf, wf, hf);
    faces.push(f);
  }

  for (let i = 0; i < hm; i++) {
    let xh = (winW/(1/((i+1)/(hm+1))))-15;
    let h = new Hand(xh, yh, wh, hh);
    hands.push(h);
  }

  for (let i = 0; i < faces.length; i++) {
    faces[i].show();
  }

  for (let i = 0; i < hands.length; i++) {
    hands[i].show();
  }

}

class Face {
  constructor(xf, yf, wf, hf) {
    this.x = xf;
    this.y = yf;
    this.w = wf;
    this.h = hf;
  }

  show() {
    image(face, this.x, this.y, this.w, this.h);
  }
}

class Hand {
  constructor(xh, yh, wh, hh) {
    this.x = xh;
    this.y = yh;
    this.w = wh;
    this.h = hh;
  }

  show() {
    image(hand, this.x, this.y, this.w, this.h);
  }
}
