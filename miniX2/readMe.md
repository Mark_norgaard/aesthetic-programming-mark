# READ ME BEFORE RUNNING MINIX2 FILE
![screenshot](Screenshot_1.png)
https://mark_norgaard.gitlab.io/aesthetic-programming-mark/miniX2/
#Controls:
h = more faces
m = decrease size
M = increase size

Hmmmm along.
further controls:

u = move up
d = move down

This code didn't quite turn out as I planned but never the less it gets the points across, in what I believe is a farily chaotic, but yet elegant manner. As the thinking faces multiply beyond the expected amount and their shapes distort. The drawing process becomes more and more difficult to keep track off, the drawing you're making should hopefully make you go "hmmmmmmm". Or in any other way become confused/interested. The program is in that sense an explanation of it self, which I believe to be quite "meta" and in line with what we have been discussing in software studies.

Otherwise I believe emojis can besides conveying a feeling or meaning, can be a fun thing to look at by them selves. 
Hopefully you enjoyed/will come to enjoy the chaotic nature of this code or more precisely it's execution.

I'm in a hurry atm. though so this is all I have time to write in this read.me

Thanks for reading ^.^
