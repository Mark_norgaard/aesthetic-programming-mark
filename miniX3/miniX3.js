
//Declaring quite a bunch of variables.
let angle = 2.0;
let offset = 300;
let scalar = 1;
let speed = 0.1;
let col = {
  r: 255,
  g: 0,
  b: 0
};
let change = 0;
let slide = 0;
let loading = 1;

function setup() {

  createCanvas(windowWidth, windowHeight);
  noStroke();
  background (0);
  rectMode(CENTER);
  //Play with lowering framerate to see the "lines" that the dots draw making up the square.
  frameRate(60);
}

function draw() {

    //Picks random colours for the dots
    col.r = random(0, 200);
    col.g = random(0, 250)
    col.b = random(100, 250);

    //This push is only for making multiple "squares", can be ignored otherwise
    push();
    // translates center points to center of the screen.
    translate(width/2,height/2);

    // Here I define two variables which I later use to decide where all the dots apear.
    // These stem from a code intended to make spirals, and if you delete the "change" variables on both the lines,
    // you can see for your self. I however, have through the magic of math and luck, used this to create a giant square
    // with some cool shapes and lines apprearing in it.
    // It should be said that if you play around with the variables after "angle" in both the x and y variable lines
    // you can create many pretty shapes. for example try letting angle stand by it self at x, and angle/2 at y.
    var x = cos(angle+change) * scalar;
    var y = sin(angle+(change/2)) * scalar;
    fill(col.r, col.g, col.b);
    noStroke();
    ellipse(x, y, 5, 5);

    //For working with push at making multiple squares.
    pop();
    //If you want to see how it would look like with another one of the squares to the right of the first one,
    // then uncomment the underlying code. Don't how why you would, but you can :).
    /*translate((((width/2)+500)),height/2);
    var x = cos(angle+change) * scalar;
    var y = sin(angle+(change/2)) * scalar;
    fill(col.r, col.g, col.b);
    noStroke();
    ellipse(x, y, 5, 5);*/

    //These two lines stand for making the "square" progress in size.
    angle += speed;
    scalar += speed;

    //These if statements together with the change variable change the spiral code from a spiral to a square.
    if (slide === 0) {
      change += 1;
      if (change === 10) {
        slide = 1;
      }
    } else {
      change -= 1;
        if (change === 0) {
          slide = 0;
        }
    }
}
