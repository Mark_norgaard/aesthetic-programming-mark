# Surveillance Snake

![screenshot](minix4_billede.png)

link: https://mark_norgaard.gitlab.io/aesthetic-programming-mark/miniX4/

controls:   
up: i 

down: k

left: j

right: l
            
*DISCLAIMER*
_This miniX I have only successfully made work in the new chromium based michrosoft edge browser. I haven't tried safari so perhaps that works. Google Chrome and firefox have not been workable for me. I don't understand why firefox doesn't work, but google chrome is due to the call for the audio input of the mic being called before user input. This seems to be a feature from google chromes side to prevent autoplaying sounds on websites, but I haven't been able to work aorund it._ It must also be said that simultaneous keypressed of directional keys is unadvised since this can sometimes make the snake turn into it self 180 degrees and die. :(

### _Submission_:
This work stresses the importance of self control and the underhanded means by which we can be robbed of it. Through the mataphor of the snake, it attempts to deceive you into thinking it is the obvious classic game, when in fact it's a listening probe that upon any knowledge of the players audiotive existence robbs the player of all control. Through this misleading teqnique the work attempts to critique surveilance culture and highlight the some times taken for granted, notion of self control, in a manipulated world.

### _Description_:
In this program I have used an already written code for the game snake found on the p5.js webpage under interactivity https://p5js.org/examples/interaction-snake-game.html. This known and broadly well understood game of snake I have then changed into being a listening probe through the players microphone. The snake is controlled with the keys "j,i,k,l". I have then used the p5.js audio library to listen for input on the users microphone and by virtue of this the game decides weather or not the user has control over the snake. Furthermore a 15 second delay before microphone activity matters is built into the code to make is more difficult to spot why the snake is suddenly out of control.
 Code wise, of notability I have used the "getLevel();", "start();", and "p5.AudioIn();" functions.

### _The Theme of capture all_:
My program adresses the theme of capture all through it's critique of the notion of self control and autonomy in a digitalised surveiled world. The player is initialy med with the notion of control but if the player makes a loud enough sound, the player looses control of the snake conpletely as it continues on outside of the players control. Further more I believe the use of a non expected form of "capturing" in the form of sound is interesting in this case. Normaly the game of snake is not connected with sound input being of importance, and the choice of sound input being the deciding factor in this work was chosen for this reason specifically. This is meant to mislead the user and is a parralel to how tech companies hide ex. microphones in devices that have no normal use for such a device, with the sole intention of spying on people.

In society this all capturing culture we are building is a relinquishment of self control and autonomy that in the wrong hands becomes a ticking bomb, where society can be manipulated and controlled, as was the case of Cambridge Analytica.
