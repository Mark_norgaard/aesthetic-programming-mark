let osc, playing, freq, amp;
let r = 1;
let root = 440
let msec = 2**(1/6);
let mthird = 2**(1/3);
let pfith = 128**(1/12);
let msix = 8**(1/4);
let pscale = [1, msec, mthird, pfith, msix, 2];
let note = pscale[0]
let notename = ['I', 'II', 'III', 'V', 'VI', 'I 2']

function setup() {
  let cnv = createCanvas(400, 400);
  cnv.position(windowWidth/2-200, windowHeight/2-200);
  cnv.mouseClicked(playOscillator);
  osc = new p5.Oscillator('sine');
  frameRate(2);
}

function draw() {
  background(220)
  //Volume set to 0.1 with a range between 0 and 1
  amp = 0.1;

  let prevnote = note;


  if (prevnote === pscale[5]) {
    nextnote = round(random(-2,0));
    note = pscale[4+nextnote]
    notenumber = 4+nextnote;
  }

    // If note is msix chooses same note or note 1 or 2 below, pfith or mthird respectively

      if (prevnote === pscale[4]) {
        nextnote = round(random(-2,1));
        note = pscale[4+nextnote]
        notenumber = 4+nextnote;
      }


    //If previous note was pfith, selects notes up to 2 below, 1 above, or it self.

      if (prevnote === pscale[3]) {
        nextnote = round(random(-2,2));
        note = pscale[3+nextnote]
        notenumber = 3+nextnote;
      }


    //If previous note was mthird, selects notes up to 2 below, 2 above, or it self.

      if (prevnote === pscale[2]) {
        nextnote = round(random(-2,2));
        note = pscale[2+nextnote]
        notenumber = 2+nextnote;
      }


    //If previous note was msec, selects notes up to 1 below, 2 above, or it self.

      if (prevnote === pscale[1]) {
        nextnote = round(random(-1,2));
        note = pscale[1+nextnote]
        notenumber = 1+nextnote;
      }


    //If previous note was the root, selects notes up to 2 above, or it self.

      if (prevnote === pscale[0]) {
        nextnote = round(random(0,2));
        note = pscale[0+nextnote]
        notenumber = 0+nextnote;
      }

      if (notenumber === 0) {
        background(255,80,125)
      } else if (notenumber === 1) {
        background(126, 101, 56);
      } else if (notenumber === 2) {
        background(10, 99, 153);
      } else if (notenumber === 3) {
        background(69, 169, 68);
      } else if (notenumber === 4) {
        background(100, 200, 200);
      } else if (notenumber === 5) {
        background(1,2,3);
      }

  freq = root * note;
  textAlign(CENTER);
  textSize(40);
  fill(255);
  text(notename[notenumber], width/2, height/2)


  textSize(15);
  text('tap to play', width/2, 20);
  text('freq: ' + round(freq) + ' hz', width/2, 40);
  text('volume: ' + amp, width/2, 60);

  if (playing) {
    // smooth the transitions by 0.2 seconds
    osc.freq(freq, 0.2);
    osc.amp(amp, 0.2);
  }

}

function playOscillator() {
  // starting an oscillator on a user gesture will enable audio
  // in browsers that have a strict autoplay policy.
  // See also: userStartAudio();
  osc.start();
  playing = true;
}

function mouseReleased() {
  // ramp amplitude to 0 over 0.5 seconds
  osc.amp(0, 0.5);
  playing = false;
}
