https://mark_norgaard.gitlab.io/aesthetic-programming-mark/miniX5/

![screenshot](minix5.jpg)

# An automatically generated melody using the A pentatonic scale

In this miniX we were tasked with creating a generative program that followed a set of rules to autonomously generate something. 
I chose to make a program which plays a "randomly" generated melody within the confines of the A pentatonic scale, stretching from A4 to A5.

### _The rules of my program_

The rules of my program consist of first an implicit rtule stating the the melody always starts on the Tonic A4. Thereafter it plays quarter notes with the exeption of repeated notes turning into half notes due to the way the program functions. Technically the note is repeated but since the oscillator has no "attack" a repeated notes simply sounds like the first note continuing. After this the notes are randomly chosen within the scale with the clause that they can never leave the confines of the octave A4-A5 and neither can the notes "jump" more than 2 steps in the scale in either direction. This leads to the program playing notes that sound "good" with eachother since they aren't randomly chosen, it also leads to the production of plausible melodies since few melodies traditionally would consist of large jumps in note values, without a musical repetition of said large jump to legitimise it's existence. The produced melodies sound as if they could fit into traditional chinese music, due to the use of the pentatonic 5 tone scale being their scale of choice, as opposed to western music using an 8 tone scale in most cases.

### _Wat role do rules and processes play in my work?_

in my miniX the rules play the role of confining the produces melodies to recognisable frequencies producing a scale, it also defines the rhythm of the melody, or perhaps since there is not deviation from said rythm it defines the lack thereof.

### _Further thoughts_

As the melody plays, to me it starts to feel like less and less like what we would traditionally call a melody. The definition of a melody cna be hard to pin point, but there seems to be a general agreement that a melody is a seperate unit from the harmony of the song and is an individually recognisable string of notes. Since my song has no harmony but only consists of a melody it live sup to the first part, but for recognizability is seems different. Te melody although immidiately recognisable seems to be just as easily forgotten as it was regocnised. It has no guaranteed repetition, no order within the confines of the scale. Due to this, it becomes more akin to noise than a  traditionally musical melody.

Perhaps the degree of autonomy in the code is too high, and the concept of the auto-generator used as a music making machine, fails because there is too much focus on the "auto" and therefor it fails to "generate", what is was initially intended to.
