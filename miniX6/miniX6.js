//Variables for setting up oscillator
let osc, playing, freq, amp;
//Variables for defining pentatonic scale in they key of A
let r = 1;
let root = 523.25
let msec = 2**(1/6);
let mthird = 2**(1/3);
let pfith = 128**(1/12);
let msix = 8**(1/4);
let pscale = [1, msec, mthird, pfith, msix, 2];
let note = pscale[0]
//Note names for filling in square.
let notename = ['I', 'II', 'III', 'V', 'VI', 'I 2']
// Variables for creating lines
let liney = 35;
let lines = 0;
let bars = 1;
let barsy = 0;
let notePositionX = 1;
let notey = 0;
let notePositionY;
//Variable for deciding if the note written needs a helping line underneath it for clarity.
let helpline = false;
let notesDrawn = 0;

//preload image for treble cleff
let cleff;
function preload() {
  cleff = loadImage('wgcleff.png');
}

function setup() {
  let cnv = createCanvas(windowWidth, windowHeight);
  cnv.mouseClicked(playOscillator);
  // make new oscillator, 'sine' can be replaced with 'sawtooth' or 'square' for different waveforms.
  osc = new p5.Oscillator('sine');
  frameRate(2);
  background(25);

}

function draw() {
  push();
  //Volume set to 0.1 with a range between 0 and 1
  amp = 0.1;

  choseNote();

  squareColour();

  //Make the info square
  rectMode(CENTER);
  rect(width/2, 800, 200,200);

  // Draw sheet paper lines.

  sheetLines();
  drawNote();


  //set frequency according to decided note, and set text in square.
  freq = root * note;
  noStroke();
  textAlign(CENTER);
  textSize(40);
  fill(255);
  text(notename[notenumber], width/2, 810)


  textSize(15);
  text('click anywhere to listen', width/2, 720);
  text('rightclick anywhere to mute', width/2, 880);
  text('freq: ' + round(freq) + ' hz', width/2, 740);
  text('volume: ' + amp, width/2, 760);


  // Glide effect from note to note and plays the oscillator at selected frequency.
  if (playing) {
    // smooth the transitions by 0.2 seconds
    osc.freq(freq, 0.2);
    osc.amp(amp, 0.2);
  }

  notesDrawn += 1;
  if (notesDrawn === 160){
    playing = false;
    osc.amp(0, 0.5);
    noLoop();
  }
}

// function for drawing notes every time a note is played,
// notePosition moves the position of each note across the x and y axis.
// It also draws helping lines for off the grid notes.
function drawNote(){
  stroke(255);
  //This one looks confusing, but simply it takes the same rules for the position of the ellipse drawn as a note
  // and draws a line underneath ot if the note would be drawn outside of the grid for visual clarity.
  if (notenumber === 3) {
    line(((width*notePositionX/32)-width/64)-8, (50+notey)-5*notePositionY, ((width*notePositionX/32)-width/64)+8, (50+notey)-5*notePositionY)
  } else if (notenumber === 4) {
    line(((width*notePositionX/32)-width/64)-8, (50+notey)-5*notePositionY, ((width*notePositionX/32)-width/64)+8, (50+notey)-5*notePositionY)
    line(((width*notePositionX/32)-width/64)-8, (50+notey)-4*notePositionY, ((width*notePositionX/32)-width/64)+8, (50+notey)-4*notePositionY)
  } else if (notenumber === 5) {
    line(((width*notePositionX/32)-width/64)-8, (50+notey)-5*notePositionY, ((width*notePositionX/32)-width/64)+8, (50+notey)-5*notePositionY)
    line(((width*notePositionX/32)-width/64)-8, (50+notey)-4*notePositionY, ((width*notePositionX/32)-width/64)+8, (50+notey)-4*notePositionY)
    line(((width*notePositionX/32)-width/64)-8, (50+notey)-3*notePositionY, ((width*notePositionX/32)-width/64)+8, (50+notey)-3*notePositionY)
  }

  noStroke();
  ellipse(((width*notePositionX/32)-width/64), (50+notey)-5*notePositionY, 10,10)
  notePositionX += 1;
  if (notePositionX === 33) {
    notePositionX = 1;
    notey += 80;
  }
}

//Function for choosing which note to play in succesion to the previous one.
function choseNote(){
  let prevnote = note;


  if (prevnote === pscale[5]) {
    nextnote = round(random(-2,0));
    note = pscale[4+nextnote]
    notenumber = 4+nextnote;
  }

// If note is msix chooses same note or note 1 or 2 below, pfith or mthird respectively
  if (prevnote === pscale[4]) {
    nextnote = round(random(-2,1));
    note = pscale[4+nextnote]
    notenumber = 4+nextnote;
  }

//If previous note was pfith, selects notes up to 2 below, 1 above, or it self.
  if (prevnote === pscale[3]) {
    nextnote = round(random(-2,2));
    note = pscale[3+nextnote]
    notenumber = 3+nextnote;
  }

//If previous note was mthird, selects notes up to 2 below, 2 above, or it self.
  if (prevnote === pscale[2]) {
    nextnote = round(random(-2,2));
    note = pscale[2+nextnote]
    notenumber = 2+nextnote;
  }

//If previous note was msec, selects notes up to 1 below, 2 above, or it self.
  if (prevnote === pscale[1]) {
    nextnote = round(random(-1,2));
    note = pscale[1+nextnote]
    notenumber = 1+nextnote;
  }

//If previous note was the root, selects notes up to 2 above, or it self.
  if (prevnote === pscale[0]) {
    nextnote = round(random(0,2));
    note = pscale[0+nextnote]
    notenumber = 0+nextnote;
  }
}

// Colouring of the square according to note played
function squareColour(){

  if (notenumber === 0) {
    fill(255,80,125)
    notePositionY = 0;
  } else if (notenumber === 1) {
    fill(126, 101, 56);
    notePositionY = 1;
  } else if (notenumber === 2) {
    fill(10, 99, 153);
    notePositionY = 2;
  } else if (notenumber === 3) {
    fill(69, 169, 68);
    notePositionY = 4;
  } else if (notenumber === 4) {
    fill(100, 200, 200);
    notePositionY = 5;
  } else if (notenumber === 5) {
    fill(180,20,36);
    notePositionY = 7;
  }
}

// function to draw sheet paper lines
function sheetLines(){
  for(let i = 0; i<5 && lines < 5; i++){

    stroke(255);
    line(0,liney, width, liney);
    for (let i = 0; i<8; i++){
      line(width*bars/8, 35+barsy, width*bars/8, 75+barsy);
      bars += 1
    }

    liney += 10
    if (i === 4) {
      image(cleff, -15, liney-55,50,50);
      liney += 30;
      lines += 1;
      bars = 1;
      barsy += 80
    }
  }
}

  //Starts oscillator on user gesture.
function playOscillator() {
  osc.start();
  playing = true;
}

function mouseReleased() {
  // ramp amplitude to 0 over 0.5 seconds
  osc.amp(0, 0.5);
  playing = false;
}
