let circleEnemies = [];
let max_circle_enemies = 3;
let players = [];
let bullets = [];
let playing = false;
let kills = 0;
let escapies = 0;
let lost = false;

function setup() {
  let cnv = createCanvas(500, windowHeight);
  cnv.mouseClicked(startGame);
  cnv.position((windowWidth/2)-250, 0);
  // make new oscillator, 'sine' can be replaced with 'sawtooth' or 'square' for different waveforms.
  frameRate(60);
  background(25);

  //Makes the Player appear on screen.
  let x1 = width/2-50;
  let x2 = width/2;
  let x3 = width/2+50;
  let y1 = height-5;
  let y2 = height-55;
  let y3 = height-5;
  let player = new Player(x1, y1, x2, y2, x3, y3);
  players.push(player);
}

function draw() {
  // sets background
  if (playing === true && lost != true) {

    background(51);
    scoreboard();
    players[0].show();


    if (keyIsDown(LEFT_ARROW)) {
      players[0].move_left();
    } else if (keyIsDown(RIGHT_ARROW)) {
      players[0].move_right();
    }

    //Calls draw circles function
    drawCircles();
    drawBullets();
    //Calls circle dead function to check if a circle should in fact be dead :'(
    circlesEscaped();
    bulletCollision();

    //Removes all bullets that have left the sreen.
    for (let i = 0; i < bullets.length; i++) {
      if (bullets[i].y1 < 0) {
        bullets.splice(i, 1);
        console.log(bullets.length);
      }
    }
  //If the game is not yet playing
} else if (playing === false && lost != true) {
    fill(255)
    textSize(20);
    noStroke();
    textAlign(CENTER)
    text("Controls:", width/2, height/5);
    text("Left arrow = Move left!", width/2, height/5+50);
    text("Right arrow = Move right!",  width/2, height/5+85);
    text("Space bar = Shoot stupid circles!", width/2, height/5+120);
    textSize(40);
    text("Click to start prooving \ntriangular superiority!", width/2, height/2);
    stroke(255,0,0);
    text("SLAY THE CIRCLES!!!", width/2, 100)
  } else if (playing === true && lost === true) {
    background(25);
    textAlign(CENTER);
    textSize(35);
    text("You have failed triangularly... \nDissapointing", width/2, height/2);
    text("Click to redeem yourself...", width/2, height/2+100);
  }
}

function mouseClicked() {
  if (lost === true && playing === true) {
    escapies = 0;
    kills = 0;
    lost = false;
    console.log("clicked");
  }
}

function scoreboard(){
  textAlign(LEFT);
  textSize(25);
  fill(255,0,0);
  text("Circles rightfully slain: " + kills, 40,50);
  text("Escaped circles...: " + escapies, 40, 80)
}
// SPACEBAR SHOOT ACTION
function keyPressed() {
  if (keyCode === 32){
    let x1 = players[0].x1 + 25;
    let x2 = players[0].x2;
    let x3 = players[0].x3-25;
    let y1 = players[0].y1 - 50;
    let y2 = players[0].y2-25;
    let y3 = players[0].y3 - 50;
    let bullet = new Bullet(x1, y1, x2, y2, x3, y3);
    bullets.push(bullet);
  }
}

// Defines starting position of enemy circles and adds them to the list circleEnemies.
function drawCircles() {
  if(circleEnemies.length < 3) {
    let x = random (10, width-10);
    let y = -300;
    let circle_enemy = new CircleEnemy(x, (y+(circleEnemies.length*100)));
    circleEnemies.push(circle_enemy);
  }
  for (let i = 0; i < circleEnemies.length; i++) {
    circleEnemies[i].show();
    circleEnemies[i].move();
  }
}

function drawBullets() {
  for (let i = 0; i < bullets.length; i++) {
    bullets[i].show();
    bullets[i].move();
  }
}

// Makes sure circles die as they should, even if they escape.
function circlesEscaped() {
  for(let i = 0; i < circleEnemies.length; i++) {
    if (circleEnemies[i].y > height) {
      circleEnemies.splice(i, 1);
      escapies += 1;
      if (escapies === 3) {
        lost = true;
      }
    }
  }
}

function bulletCollision() {
  for (let i = 0; i < bullets.length; i++) {
    let xb = bullets[i].x2;
    let yb = bullets[i].y2-15;
    for (let j = 0; j < circleEnemies.length; j++){
      let xc = circleEnemies[j].x;
      let yc = circleEnemies[j].y;
      if (abs(xb-xc) < 25 && abs(yb-yc) < 5) {
        bullets.splice(i, 1);
        circleEnemies.splice(j, 1);
        kills += 1;
      }
    }
  }
}

class CircleEnemy {
  constructor (x_circle, y_circle) {
    this.x = x_circle;
    this.y = y_circle;
  }
  move() {
    this.y = this.y +2;
  }

  show() {
    stroke(50);
    fill(255);
    ellipseMode(CENTER);
    ellipse(this.x, this.y, 20,20);
  }
}

class Player {
  // Defines triangle shapes needed variabels.
  constructor (x1_player, y1_player, x2_player, y2_player, x3_player, y3_player){
    this.x1 = x1_player;
    this.x2 = x2_player;
    this.x3 = x3_player;

    this.y1 = y1_player;
    this.y2 = y2_player;
    this.y3 = y3_player;
  }
  //Moves the player left and right
  move_right() {
    if(this.x2 < width) {
      this.x1 += 3;
      this.x2 += 3;
      this.x3 += 3;
    }
  }
  move_left() {
    if (this.x2 > 0) {
      this.x1 -= 3;
      this.x2 -= 3;
      this.x3 -= 3;
    }
  }
  show() {
    stroke(50);
    fill(255,0,0);
    triangle(this.x1, this.y1, this.x2, this.y2, this.x3, this.y3);
  }
}

class Bullet {
  constructor(x1_bullet, y1_bullet, x2_bullet, y2_bullet, x3_bullet, y3_bullet) {
    this.x1 = x1_bullet;
    this.x2 = x2_bullet;
    this.x3 = x3_bullet;

    this.y1 = y1_bullet;
    this.y2 = y2_bullet;
    this.y3 = y3_bullet;
  }
  show() {
    stroke(50);
    fill(0,255,0);
    triangle(this.x1, this.y1, this.x2, this.y2, this.x3, this.y3);
    //triangle(players[0].x1 + 25, players[0].y1 + 50, players[0].x2, players[0].y2+25, players[0].x3-25, players[0].y3 + 50)
  }
  move() {
    this.y1 -= 5;
    this.y2 -= 5;
    this.y3 -= 5;
  }
}

function startGame() {
  playing = true;
}
