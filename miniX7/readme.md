[RunMe](https://mark_norgaard.gitlab.io/aesthetic-programming-mark/miniX7/)

![screenshot](Screenshot_1.png)
### Describe your program

My program this week is a game all about destroying circles for triangular superiority. In simpler terms, it's a game where you control a triangle and shoot appreaching circles down with green triangles in order to prove that trianges are in fact the superior shape. The game has three states: 
1. The player has just opened the game and it is yet to begin, a missions of slaying cirlces is given and controls are displayed. The player is here also promted to click with their mouse to start.

2. The player starts the game and shoots down the circles until the amount of escaping circles goes above 3.

3. The player has failed "triangularly" (because I have no sense of humor), and is promted to click their mouse again to restart.

All these three states are on different screens so to speak, as seen on the image of my having edited them all side by side in order.

### How do my programs objects function?

My objects function through first of all their given properties which define where on the screen they will be shown and are, which is important for handling collisions between different objects and objects exiting the boundaries of the game screen.
Secondarily they function through their behaviors/methods. These consists of being able to move and being drawn on the canvas. Further functionality of objects such as the player being able to shoot, is not a part of the players object class, but a part of interconnecting code which gives the objects functionality in relation to eachother and inputs form the user.

### What are the characteristics of object oriented programming? 

The biggest characteristic I got from the assigned reading was that objects in programming are a product of abstraction much like captured data. I think of a triangle in the real world, abstract the properties of such a triangle and then I further abstract whatb it is capable of doing. This lets me bend the boundaries of what it means to be a triangle, since I as a programmer am allowed acess to these extra levels of object abstraction.
The connection between real world phenomena and abstract phenomena as marx points out is central to understanding object abstraction as it provides an insight into where these abstractions take root in the real world.

### Connection to real world phenomena.
In my program some of this connection between the abstracted objects and real world phenomena come into play in the way I have decided to word and introduce the different objects. The circles are made as objects attempting to escape, the concept of escaping might be abstracted as something more innocent. The triangles shooting down the escaping circles ruthlessly with a wording of "circles rightfuly slain", attempts to suggest a sort of righteus retribution, where the triangles feel a strong reason to slay the circles. The object abstraction I have created thus draws heavily on real world phenomena and antagonist relations. Be they racial, political og religious.




