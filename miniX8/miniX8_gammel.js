let words = ['hej', 'med', 'dig', 'Michael', 'du', 'ser', 'godt', 'ud', 'i', 'dag'];
let counter = words.length;
let x_offset = 5;
let y_offset = 20;
let word_n = 0;

// function preload() {
//   speaker = loadSound('assets/speaker_person.mp3');
// }

function setup(){
  let cnv = createCanvas(windowWidth, windowHeight);
  background(50);
  textSize(5);
  frameRate(2);
}

function draw() {


    fill(255,200,0);
    text(words[word_n], x_offset, y_offset);

    if (word_n > 0) {
      fill(255);
      text(words[word_n -1], x_offset-15, y_offset);
    }

    word_n += 1;
    x_offset += 15;
    // y_offset += 10;


}
