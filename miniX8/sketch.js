let thispoemBegin;
let framecount = 0;
let wordsSpoken = 0;
let waitTime = 0;
let waiting = true;
y = 90;// counter/ start pos. for the arrow-reader

/*the this function preloads the data from
the json file and connects it to the variabel:"thisPoemBegin"
*/

function preload() { //Preloads all the different soundfiles and the JSON file containing words and positions.
  thisPoemBegin = loadJSON('withVersesWithin.json');
  sayFunction = loadSound("soundfiles/function.wav");
  sayPreload = loadSound("soundfiles/preload.wav");
  sayParenthesis = loadSound("soundfiles/parenthesis.wav");
  sayFunctionstart = loadSound("soundfiles/functionstart.wav");
  sayThisPoemBegin = loadSound("soundfiles/thispoembegin.wav");
  sayEquals = loadSound("soundfiles/equals.wav");
  sayLoadJSON = loadSound("soundfiles/loadjson.wav");
  sayParenthesis2 = loadSound("soundfiles/parenthesis2.wav");
  sayWithVersesWithin = loadSound("soundfiles/withverses.wav");
  sayParenthesisEnd = loadSound("soundfiles/parenthesisend.wav");
  sayFunctionEnd = loadSound("soundfiles/functionend.wav");
}

function setup() {
  createCanvas(windowWidth, windowHeight)

}

function draw() {

  background("#333");
  //lineRead();// calls function
  poem();// calls function

}

//This is the function that controlls the removed curly bracket.
/*function lineRead() {

  text("}", 10, 3 + y);// curly-bracket arrow

  y = y + 0.2;// the speed
  if (y > height) {// start condition

    y = 90;// reset
  }

}*/

function poem() {


  fill("#White");
  push();
  textFont("Gregoria");
  textSize(40);
  text("Vocal load", 60, 50);
  pop();
  textSize(20);
  textFont("Monaco");


  /*text syntax that first refers to the name of json.file, followed by the
[index number] followed by ((.)dot notation) and finally the property
  */

  // makes the program wait playing untill the user has pressed the mouse.
  if (waiting == true) {
    waitTime += 1;
  }


  if (framecount > 1 + waitTime) {
  text(thisPoemBegin[0].rhyme, thisPoemBegin[0].x_pos, thisPoemBegin[0].y_pos);
  if (wordsSpoken == 0) {
    sayFunction.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 48 + waitTime) {
  text(thisPoemBegin[1].rhyme, thisPoemBegin[1].x_pos, thisPoemBegin[1].y_pos);
  if (wordsSpoken == 1) {
    sayPreload.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 90 + waitTime) {
  text(thisPoemBegin[2].rhyme, thisPoemBegin[2].x_pos, thisPoemBegin[2].y_pos);
  if (wordsSpoken == 2) {
    sayParenthesis.play();
    wordsSpoken += 1;
    }
  }

  // text(thisPoemBegin[2].rhyme, thisPoemBegin[2].x_pos, thisPoemBegin[2].y_pos);

  if (framecount > 175 + waitTime) {
  text(thisPoemBegin[3].rhyme, thisPoemBegin[3].x_pos, thisPoemBegin[3].y_pos);
  if (wordsSpoken == 3) {
    sayFunctionstart.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 250 + waitTime) {
  text(thisPoemBegin[5].rhyme, thisPoemBegin[5].x_pos, thisPoemBegin[5].y_pos);
  if (wordsSpoken == 4) {
    sayThisPoemBegin.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 350 + waitTime) {
  text(thisPoemBegin[6].rhyme, thisPoemBegin[6].x_pos, thisPoemBegin[6].y_pos);
  if (wordsSpoken == 5) {
    sayEquals.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 415 + waitTime) {
  text(thisPoemBegin[7].rhyme, thisPoemBegin[7].x_pos, thisPoemBegin[7].y_pos);
  if (wordsSpoken == 6) {
    sayLoadJSON.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 500 + waitTime) {
  text(thisPoemBegin[8].rhyme, thisPoemBegin[8].x_pos, thisPoemBegin[8].y_pos);
  if (wordsSpoken == 7) {
    sayParenthesis2.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 580 + waitTime) {
    if (wordsSpoken == 8) {
      sayWithVersesWithin.play();
      wordsSpoken += 1;
      }
  }

  if (framecount > 750 + waitTime) {
  if (wordsSpoken == 9) {
    sayParenthesisEnd.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 840 + waitTime) {
  text(thisPoemBegin[4].rhyme, thisPoemBegin[4].x_pos, thisPoemBegin[4].y_pos);
  if (wordsSpoken == 10) {
    sayFunctionEnd.play();
    wordsSpoken += 1;
    }
  }

  if (framecount > 950 + waitTime) {
  noLoop();
  }

  framecount += 1;
}

// This function bypasses the autoplay policy of browsers such as chrome and safari
function mousePressed(){
  userStartAudio();
  waiting = false;
}
