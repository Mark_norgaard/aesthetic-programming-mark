**Screenshot:**
<br>
![](https://gitlab.com/celia_w/AP2021/-/raw/master/minix/miniX9/Screenshot_2021-04-18_at_15.30.48.png)

**Program link at Celia's gitlab (group project):**
<br>
[Click here!](https://celia_w.gitlab.io/AP2021/minix/miniX9/)

**Code link:**
<br>
[Click here!](https://gitlab.com/celia_w/AP2021/-/blob/master/minix/miniX9/sketch.js)


**What is the program about? Which API have you used and why?**

Our program works with two APIs: Danmarks Statistik and Visual Crossing’s Weather API.

The date is picked by using a drop down, which then is added to the API’s URL to find the number of people with the birthday on that date, as well as the weather for that date. The weather is then represented with text as well as the data for things such as temperature, what it felt like and precipitation.

The texts were written with the text function, as well as calling for the specific data from the API, such as here: “text("The temperature was " + weather.days[0].temp + " ℃", 50,200);” The date was a bit more difficult to figure out, since one of the API’s needed the date with a 0 in front, if it was below 10, and so we used a for-loop and an if-else statement to fix this. This gave us a lot of variables to work with. 

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data?**

We acquire the data we make use of through two separate API services. One is hosted by “danmarks statistik”, the other by “visual crossing” who can be read about here (https://www.visualcrossing.com/about). How these two services acquire their data we do not know, which raises some interesting questions to the believability of the questions and why data presented in API’s for us as a group have more merit than data represented by a person on the streets or through personal memory. 

If big data corporations and search queries work together to present data in distorted ways due to the ideologies of the people who have aggregated the data and their methods for doing so, how can we trust that, for example the weather data we use is in fact accurate? A question could be asked as to a motivation for saving false weather data and hosting it on an API, but also a question could be asked as to whether or not the validity of the weather data matters in our context. We are using the data to display what the weather was like on a person's birthday. Together with this the common superstition that “if a person was good this year, they will have nice weather on their birthday”. Of course this isn’t true, but it raises a question of classification of the people who had their birthday on a specific day. Does this mistaking of correlation as causation lead to any information, true or false about a class of people. Of course it does. First of all there is the potential for false classification of a group of people as bad people due to their birthday falling on a day with poor weather or the opposite. This program shows that even if the acquisition of data is correct, and the processing and representation of the data also is (we don’t lie and show different data than the data acquired from the API), the potential use of the data to categorise people in a class can be nefariously abused to stigmatise everyone born on a specific date. This program uses data in order to extremify how data, even if true, can be used for many purposes, and makes us think about what data is being used for the wrong purposes in our society. We focus here on how mistreatment of data can seemingly prove something that is known to be false, such as when a single variable analysis of data is made to say that “people in this area of town commit fewer crimes because they are white, and people in this area commit more because they are black”, instead of querying into why the people of the black community are having a harder time and perhaps feel socially pressured to commit crime.

**How much do you understand this data or what do you want to know more about?**

We had to look through all of the data from the weather API and sort out what we wanted to be shown in our program, but there was some difficulty as well in understanding some of the data. For example, we wanted to show what the wind speed had been on a given day in Aarhus, but the numbers shown were too high to be realistic, even though we had asked for it in metric. The API did not show any measuring units as well, and because of this we decided to just not show the wind speed at all. It would have been nice to have more clear-cut, easily understandable data from the API. This experience shows that even if you are capable of acquiring data from an API a certain amount of expertise and understanding of the data is still required, to further process it. 

**How do platform providers sort the data and give you the requested data? What are the power relations in the chosen APIs?**


When opening the data files on firefox (or the specific URL), it’s already neatly set up as a JSON file. Chrome can do the same, but requires an extension. It was set up in a very neat way, which made it easy to figure out the different things you wanted to know - the temperature, wind measurements, precipitation and more was all under one category. Usually, you can find many different categories and setups in APIs, but ours was pretty straightforward. 

**What is the significance of APIs in digital culture?**

A thing API’s manage to do is distribute data that formerly was impossible to access in large quantities. To know the weather for an entire year you might have had to collect newspapers for that entire year and categorise them, and even then you would rely on forecasts which are notoriously imprecise. If you wanted to know how many people travel from one place to another by specific bikes from a bike loaning company, you could never do such a thing. With the power of API’s and digital data collecting this all becomes possible. Furthermore the API’s that are available are often free to use within a limit, meaning many tables of data are now accessible to people who aren’t in the corporation which collected the data. This opens up opportunities that only the corporation itself had before as it attributes further liberty to the data. 
Going back to the weather example, this plethora of data available lets us as consumers track for example global warming around the globe by ourselves. We can aggregate our own data sets and do our own research if we so wanted. In the same way that for example online books and pdf files let millions of people have their own copy of a book, where before perhaps only thousands of copies of the book existed, the API let’s everyone with an internet connection have access to massive datasets and opportunities for learning and creating.

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**

Is it possible to change the setup of a JSON file, or how do they decide on the setup and categories?

**References:**
[Danmarks Statistik, API](https://www.dst.dk/da/Statistik/brug-statistikken/muligheder-i-statistikbanken/api)
<br>
[Danmarks Statistik, birthdays](https://www.dst.dk/da/informationsservice/oss/foedselsdag)
<br>
[Weather API from Visual Crossing](https://www.visualcrossing.com/weather-api)


**Bibliography:**
<br>
Soon Winnie & Cox, Geoff, "Que(e)ry data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 186-210
<br>
Eric Snodgrass and Winnie Soon, “API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange],”First Monday 24, no.2 (2019)

