let x = 0;
let y = 0;
let spacing = 10;
let r, g, b;


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);

}

function draw() {
  r = random(255);
  g = random(255);
  b = random(255);
  stroke(r,g,b);
  if (random(1) < 0.5) {
    line(x, y, x+spacing, y+spacing);
  } else {
    line(x, y+spacing, x+spacing, y);
  }

  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
